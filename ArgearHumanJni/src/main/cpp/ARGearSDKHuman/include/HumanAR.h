/*******************************************************************************
 * Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of
 * SEERSLAB Inc. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license
 * agreement you entered into with SEERSLAB Inc.
 *
 * "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 ******************************************************************************/

/**
 *
 *  @file       HumanAR.h
 *  @brief      Declaration of ARGear human AR APIs
 *  @author     mj < mg.jeong@seerslab.com >
 *  @author     Hyomin Kim < hm@seerslab.com >
 *  @author     minwoo kim < kmw4988@seerslab.com >
 *
 */

#ifndef ARGEAR_SDK_HUMAN_AR_H_
#define ARGEAR_SDK_HUMAN_AR_H_

#include <list>
#include <map>
#include <memory>

#include "ARGearSDKTypes/include/Frame.h"
#include "HumanARConfig.h"
#include "HumanARTypes.h"

namespace argear {
namespace sdk {

namespace content {
class ContentInfo;
class CMSInterface;
}  // namespace content

namespace mlcore {
class MLProcess;
}  // namespace mlcore

namespace human {

/**
 * @brief
 *
 */
class HumanAR {
 public:
  HumanAR();
  ~HumanAR();

  // setup the configuration including CMS information
  bool SetConfiguration(HumanARConfig config);

  // Initialize the AR categories and load ML models to the memory
  bool Initialize(HumanARCategory categories);

  // Finalize the AR categories and unload ML models from the memory
  bool Finalize(HumanARCategory categories);

  // Enable target AR categories.
  bool EnableCategory(HumanARCategory categories);

  // Disable target AR categories. The ML models doesn't be unloaded from the
  // memory
  bool DisableCategory(HumanARCategory categories);

  // AR process
  /**
   * @brief Process with image frame
   *
   * @param result_frame [OUT] 
   * @param image_frame [IN] 
   * @param options [IN] 
   */
  void Process(ARGearResultFrameData& result_frame,
               const ARGearImageFrameData& image_frame,
               HumanARProcessOptions options = HumanARProcessOptions::none_);

  /**
   * @brief Process with camera frame
   *
   * @param result_frame [OUT] 
   * @param camera_frame [IN] 
   * @param options [IN] 
   */
  void Process(ARGearResultFrameData& result_frame,
               const ARGearCameraFrameData& camera_frame,
               HumanARProcessOptions options = HumanARProcessOptions::none_);

  /**
   * @brief Get the Content Category object
   *
   * @param key [IN] parent category name, if the key equals "", then it means
   *            root level category
   * @return std::list<std::string> the list of category
   */
  std::list<std::string> GetContentCategory(const std::string& key = "");

  // TODO: consider the asynchronous to get the content list due to the network
  // operation
  /**
   * @brief Get the Content List object
   *
   * @param contents [OUT] retrieval list of content information
   * @param category [IN] category name
   * @param offset [IN] starting posting to retrieve
   * @param count [IN] the requested number of content
   * @return unsigned int actual retrieved number of content
   */
  unsigned int GetContentList(
      std::list<argear::sdk::content::ContentInfo>& contents,
      const std::string& category, unsigned int offset, unsigned int count);

  /**
   * @brief
   *
   * @param uri
   */
  // TODO: consider application gets the content image and applies it directly,
  //       the the caching can be possible. Or ARGear SDK enables cache.
  //       consider callback regisration for content download finishing.
  /**
   * @brief Get the Content object
   *
   * @param uri [IN] content URI
   */
  void GetContent(const std::string& uri);

  // TODO: consider callback registration for the content effects
  void ApplyContent(const std::string& uuid);

  /**
   * @brief
   *
   * @param content_path if "", all clear
   */
  void CancelContent(const std::string& uuid);

 private:
  // register MLProcesses according to the AR categories
  void RegisterMLProcess(HumanARCategory categories);

  // Unregister ML processes according to the AR categories
  void UnregisterMLProcess(HumanARCategory categories);

  HumanARConfig config_;
  std::shared_ptr<argear::sdk::content::CMSInterface> cms_;
  std::map<int, std::shared_ptr<argear::sdk::mlcore::MLProcess> > ml_processes_;
  // std::shared_ptr<argear::sdk::renderer::RendererPipeline>
  // renderer_pipeline_;
};  // class HumanAR

}  // namespace human
}  // namespace sdk
}  // namespace argear

#endif  // ARGEAR_SDK_HUMAN_AR_H_
/*******************************************************************************
 * Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of
 * SEERSLAB Inc. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license
 * agreement you entered into with SEERSLAB Inc.
 *
 * "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 ******************************************************************************/

/**
 *
 *  @file       HumanARTypes.h
 *  @brief      Declaration of type information for ARGear human AR.
 *  @author     mj < mg.jeong@seerslab.com >
 *  @author     Hyomin Kim < hm@seerslab.com >
 *  @author     minwoo kim < kmw4988@seerslab.com >
 *
 */

#ifndef ARGEAR_SDK_HUMAN_AR_TYPES_H_
#define ARGEAR_SDK_HUMAN_AR_TYPES_H_

namespace argear {
namespace sdk {
namespace human {

/**
 * @brief
 *
 */
enum class HumanARCategory : int {
  none_ = 0x0000,
  face_ = 0x0001,
  beautification_ = 0x0002,
  bg_segmentation_ = 0x0004,
  face_segmentation_ = 0x0008,
  foot_fitting_ = 0x0010,
  glass_fitting_ = 0x0020,
  all_ = 0xFFFF,
};

/**
 * @brief
 *
 */
enum class HumanARProcessOptions : int {
  none_ = 0x0000,
  
};

}  // namespace human
}  // namespace sdk
}  // namespace argear

#endif  // ARGEAR_SDK_HUMAN_AR_TYPES_H_
/*******************************************************************************
 * Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of
 * SEERSLAB Inc. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license
 * agreement you entered into with SEERSLAB Inc.
 *
 * "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 ******************************************************************************/

/**
 *
 *  @file       HumanARConfig.h
 *  @brief      Declaration of configurartion for ARGear human AR
 *  @author     mj < mg.jeong@seerslab.com >
 *  @author     Hyomin Kim < hm@seerslab.com >
 *  @author     minwoo kim < kmw4988@seerslab.com >
 *
 */

#ifndef ARGEAR_SDK_HUMAN_AR_CONFIG_H_
#define ARGEAR_SDK_HUMAN_AR_CONFIG_H_

namespace argear {
namespace sdk {
namespace human {

// TODO: all parameteres should be parsed from the configuration file( text
// format )

class HumanARConfig {
 public:
  HumanARConfig(){}
  ~HumanARConfig(){}

  void SetLogFile(const std::string& log_file) { this->log_file_ = log_file; }
  void SetCMSURL(const std::string& url) { this->cms_url_ = url; }
  void SetCompressedContentPath(const std::string& path) {
    this->compressed_content_path_ = path;
  }
  void SetUncompressedContentPath(const std::string& path) {
    this->uncompressed_content_path_ = path;
  }

  const std::string& GetLogFile(void) { return this->log_file_; }
  const std::string& GetCMSURL(void) { return this->cms_url_; }
  const std::string& GetCompressedContentPath(void) {
    return this->compressed_content_path_;
  }
  const std::string& GetUncompressedContentPath(void) {
    return this->uncompressed_content_path_;
  }

 private:
  std::string log_file_;
  std::string cms_url_;
  std::string compressed_content_path_;
  std::string uncompressed_content_path_;
};

}  // namespace human
}  // namespace sdk
}  // namespace argear

#endif  // ARGEAR_SDK_HUMAN_AR_CONFIG_H_
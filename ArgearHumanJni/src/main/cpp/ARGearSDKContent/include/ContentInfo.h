/*******************************************************************************
 * Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of
 * SEERSLAB Inc. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license
 * agreement you entered into with SEERSLAB Inc.
 *
 * "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 ******************************************************************************/

/**
 *
 *  @file       ContentInfo.h
 *  @brief      Declaration of ContentInfo provided from CMS
 *  @author     mj < mg.jeong@seerslab.com >
 *  @author     Hyomin Kim < hm@seerslab.com >
 *  @author     minwoo kim < kmw4988@seerslab.com >
 *
 */

#ifndef ARGEAR_SDK_CONTENT_INFO_H_
#define ARGEAR_SDK_CONTENT_INFO_H_

#include <map>

namespace argear {
namespace sdk {
namespace content {

/**
 * @brief
 *
 */
class ContentInfo {
 public:
  // TODO: consider this constructor is needed
  ContentInfo() {}

  // TODO: consider the initializing parameters.
  ContentInfo(std::string uuid, std::string uri, std::string thumbnail_uri)
      : uuid_(uuid), content_uri_(uri), thumbnail_uri_(thumbnail_uri) {
    this->properties_.clear();
  }

  ~ContentInfo() { this->properties_.clear(); }

  // TODO: consider the public setter APIs for properties.
  //       if the default constructor is nor required, these setter APIs are not
  //       requried.

  /**
   * @brief Set the content UUID
   *
   * @param uuid
   */
  void SetUUID(const std::string& uuid) { this->uuid_ = uuid; }

  /**
   * @brief Set the content URI
   *
   * @param uri
   */
  void SetURI(const std::string& uri) { this->content_uri_ = uri; }

  /**
   * @brief Set the thumbnail iamge URI
   *
   * @param uri
   */
  void SetThumbnailURI(const std::string& uri) { this->thumbnail_uri_ = uri; }

  /**
   * @brief Set the content property
   *
   * @param key key string
   * @param val value string
   */
  void SetProperty(const std::string& key, const std::string& val) {
    this->properties_.insert(std::pair<std::string, std::string>(key, val));
  }

  /**
   * @brief Get the content UUID
   *
   * @return uuid
   */
  const std::string& GetUUID(void) { return this->uuid_; }

  /**
   * @brief Get the URI string of content ZIP file
   *
   * @return uri
   */
  const std::string& GetURI(void) { return this->content_uri_; }

  /**
   * @brief Get the URI string for the thumbnail iamge
   *
   * @return uri
   */
  const std::string& GetThumbnailURI(void) { return this->thumbnail_uri_; }

  /**
   * @brief Get the contnet property string according to the key string
   *
   * @param key key string
   * @return value
   */
  const std::string& GetAttribute(std::string key) {
    return this->properties_.at(key);
  }

 private:
  std::string uuid_;
  std::string content_uri_;
  std::string thumbnail_uri_;
  std::map<std::string, std::string> properties_;
};  // class ContentInfo

}  // namespace content
}  // namespace sdk
}  // namespace argear

#endif  // ARGEAR_SDK_CONTENT_INFO_H_
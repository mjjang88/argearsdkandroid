/*******************************************************************************
* Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
*
* PROPRIETARY/CONFIDENTIAL
*
* This software is the confidential and proprietary information of
* SEERSLAB Inc. You shall not disclose such Confidential Information
* and shall use it only in accordance with the terms of the license
* agreement you entered into with SEERSLAB Inc.
*
* "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
* OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
* IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
* NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
* SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
* SOFTWARE OR ITS DERIVATIVES.
******************************************************************************/

#include <jni.h>

#include "ARGearSDKHuman/include/HumanAR.h"

using namespace argear;
using namespace sdk;
using namespace human;

HumanAR* humanAr = nullptr;

const char* GetString(JNIEnv *env, jobject source_object, const char* field_name, jstring* jstr_field) {
    jclass data_class = env->GetObjectClass(source_object);

    *jstr_field = (jstring)(env->GetObjectField(
            source_object,
            env->GetFieldID(data_class, field_name, "Ljava/lang/String;")));

    return env->GetStringUTFChars(*jstr_field, nullptr);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_seerslab_argear_ARGear_create(JNIEnv *env, jobject thiz) {

    humanAr = new HumanAR();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_seerslab_argear_ARGear_destroy(JNIEnv *env, jobject thiz) {

    delete humanAr;
    humanAr = nullptr;
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_seerslab_argear_ARGear_setConfiguration(JNIEnv *env, jobject thiz,
                                                 jobject human_arconfig) {

    jstring jstr_logfile_path, jstr_cms_url, jstr_compressed_content_path, jstr_un_compressed_content_path;

    const char* str_logfile_path = GetString(env, human_arconfig, "_logFilePath", &jstr_logfile_path);
    const char* str_cms_url = GetString(env, human_arconfig, "_cmsUrl", &jstr_cms_url);
    const char* str_compressed_content_path = GetString(env, human_arconfig, "_compressedContentPath", &jstr_compressed_content_path);
    const char* str_un_compressed_content_path = GetString(env, human_arconfig, "_unCompressedContentPath", &jstr_un_compressed_content_path);

    HumanARConfig *humanArConfig = new HumanARConfig();
    humanArConfig->SetLogFile(str_logfile_path);
    humanArConfig->SetCMSURL(str_cms_url);
    humanArConfig->SetCompressedContentPath(str_compressed_content_path);
    humanArConfig->SetUncompressedContentPath(str_un_compressed_content_path);

    bool ret = false;
    if (humanAr != nullptr) {
        ret = humanAr->SetConfiguration(*humanArConfig);
    }

    delete humanArConfig;
    env->ReleaseStringUTFChars(jstr_logfile_path, str_logfile_path);
    env->ReleaseStringUTFChars(jstr_cms_url, str_cms_url);
    env->ReleaseStringUTFChars(jstr_compressed_content_path, str_compressed_content_path);
    env->ReleaseStringUTFChars(jstr_un_compressed_content_path, str_un_compressed_content_path);

    return ret;
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_seerslab_argear_ARGear_initialize(JNIEnv *env, jobject thiz, jint categories) {

    bool ret = false;
    if (humanAr != nullptr) {
        ret = humanAr->Initialize((HumanARCategory)categories);
    }

    return ret;
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_seerslab_argear_ARGear_finalize(JNIEnv *env, jobject thiz, jint categories) {

    bool ret = false;
    if (humanAr != nullptr) {
        ret = humanAr->Finalize((HumanARCategory)categories);
    }

    return ret;
}
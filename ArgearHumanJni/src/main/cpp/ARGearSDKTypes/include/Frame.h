/*******************************************************************************
 * Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of
 * SEERSLAB Inc. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license
 * agreement you entered into with SEERSLAB Inc.
 *
 * "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 ******************************************************************************/

/**
 *
 *  @file       Frame.h
 *  @brief      typedef of Frame
 *  @author     Hyomin Kim < hm@seerslab.com >
 *
 */

#ifndef ARGEAR_SDK_TYPES_FRAME_H_
#define ARGEAR_SDK_TYPES_FRAME_H_

#include <string>

namespace argear {
namespace sdk {

/**
 * @brief
 *
 */
enum class ImageFormat : int {
  kImageFormatNone = 0,   /* NONE */
  kImageFile,             /* Image file path */
  kImageFormatTextureId,  /* OpenGL Texture ID */
  kImageFormatYuvNV21,    /* Android Camera API1(YUV420sp) */
  kImageFormatYuv420_888, /* Android Camera API2 (YUV_420_888, YUV420p) */
  kImageFormatRGB,        /* RGB format */
  kImageFormatRGBA,       /* RGBA format */
};

/**
 * @brief
 *
 */
typedef struct tag_ARGearImageFrameData {
  std::string file_path = std::string();
  unsigned char* buffer = nullptr;
  int texture_id = 0;
  int width = 0;
  int height = 0;
  ImageFormat format = ImageFormat::kImageFormatNone;
} ARGearImageFrameData;

/**
 * @brief
 *
 */
typedef struct tag_ARGearCameraConfig {
  int width = 0; 
  int height = 0;
  ImageFormat format = ImageFormat::kImageFormatNone;
  bool is_front_facing = false;
  int orientation = 0;
  float horizontal_fov = 0;
  float vertical_fov = 0;
} ARGearCameraConfig;
/**
 * @brief
 *
 */
typedef struct tag_ARGearCameraFrameData {
  ARGearCameraConfig camera_config;
  long time = 0;
  unsigned char* buffer = nullptr;
  int fbo = 0;
  int tex = 0;
} ARGearCameraFrameData;

/**
 * @brief
 *
 */
typedef struct tag_ARGearResultFrameData {
  unsigned char* buffer = nullptr;
  int texture_id = 0;
  int width = 0;
  int height = 0;
  ImageFormat format = ImageFormat::kImageFormatNone;
} ARGearResultFrameData;

} // namespace sdk
} // namespace argear

#endif // ARGEAR_SDK_TYPES_FRAME_H_

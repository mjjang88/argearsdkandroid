package com.seerslab.argear.model;

/*******************************************************************************
 * Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of
 * SEERSLAB Inc. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license
 * agreement you entered into with SEERSLAB Inc.
 *
 * "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 ******************************************************************************/

public class HumanARConfig {
    private String _logFilePath;
    private String _cmsUrl;
    private String _compressedContentPath;
    private String _unCompressedContentPath;

    public HumanARConfig(String logfilePath, String cmsUrl, String compressedContentPath, String unCompressedContentPath) {
        _logFilePath = logfilePath;
        _cmsUrl = cmsUrl;
        _compressedContentPath = compressedContentPath;
        _unCompressedContentPath = unCompressedContentPath;
    }

    public void setLogFilePath(String logFilePath) {
        _logFilePath = logFilePath;
    }

    public String getLogFilePath() {
        return _logFilePath;
    }

    public void setCmsUrl(String cmsUrl) {
        _cmsUrl = cmsUrl;
    }

    public String getCmsUrl() {
        return _cmsUrl;
    }

    public void setCompressedContentPath(String compressedContentPath) {
        _compressedContentPath = compressedContentPath;
    }

    public String getCompressedContentPath() {
        return _compressedContentPath;
    }

    public void setUnCompressedContentPath(String unCompressedContentPath) {
        _unCompressedContentPath = unCompressedContentPath;
    }

    public String getUnCompressedContentPath() {
        return _unCompressedContentPath;
    }
}

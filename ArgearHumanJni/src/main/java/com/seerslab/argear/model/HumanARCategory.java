package com.seerslab.argear.model;

/*******************************************************************************
 * Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of
 * SEERSLAB Inc. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license
 * agreement you entered into with SEERSLAB Inc.
 *
 * "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 ******************************************************************************/

public enum HumanARCategory {
    NONE(0x0000),
    FACE(0x0001),
    BEAUTIFICATION(0x0002),
    BG_SEGMENTATION(0x0004),
    FACE_SEGMENTATION(0x0008),
    FOOT_FITTING(0x0010),
    GLASS_FITTING(0x0020),
    ALL(0xFFFF);

    private final int _value;

    HumanARCategory(int value) {
        _value = value;
    }

    public int getValue() {
        return _value;
    }
}

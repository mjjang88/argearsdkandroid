package com.seerslab.argear;

/*******************************************************************************
 * Copyright(C) 2021 SEERSLAB Inc. All Rights Reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of
 * SEERSLAB Inc. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license
 * agreement you entered into with SEERSLAB Inc.
 *
 * "SEERSLAB Inc." MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. "SEERSLAB Inc." SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 ******************************************************************************/

import com.seerslab.argear.model.HumanARCategory;
import com.seerslab.argear.model.HumanARConfig;

import java.util.Iterator;
import java.util.Set;

public class ARGear {
    static {
        System.loadLibrary("argear-jni");
    }

    public native void create();
    public native void destroy();

    public native boolean setConfiguration(HumanARConfig humanARConfig);
    private native boolean initialize(int categories);
    public boolean initialize(Set<HumanARCategory> categories) {

        if (categories == null) return false;

        int categoryValue = getCategoryValue(categories);
        return initialize(categoryValue);
    }
    private native boolean finalize(int categories);
    public boolean finalize(Set<HumanARCategory> categories) {

        if (categories == null) return false;

        int categoryValue = getCategoryValue(categories);
        return finalize(categoryValue);
    }




    private int getCategoryValue(Set<HumanARCategory> categories) {

        int categoryFlag = 0;
        for (HumanARCategory category : categories) {
            categoryFlag += category.getValue();
        }

        return categoryFlag;
    }
}
